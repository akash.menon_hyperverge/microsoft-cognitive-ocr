const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");
const path = require("path");

const { IMAGE_DIRECTORY, ENDPOINT, API_KEY } = process.env;

const sendRequest = (filename) => {
  const form = new FormData();
  const filePath = path.join(IMAGE_DIRECTORY, filename);

  form.append("image_data", fs.createReadStream(filePath));

  return axios.default.post(ENDPOINT, form, {
    params: {
      language: "unk",
      detectOrientation: "true",
    },
    headers: {
      "Ocp-Apim-Subscription-Key": API_KEY,
      ...form.getHeaders(),
    },
  });
};

module.exports = sendRequest;
