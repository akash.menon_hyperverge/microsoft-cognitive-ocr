require("dotenv").config();
const writeToCsv = require("./csvHelper");
const sendRequest = require("./apiHelper");
const getFileData = require("./txtHelper");

const sleep = (seconds) => {
  return new Promise((resolve) => setTimeout(resolve, seconds * 1000));
};

const main = async () => {
  const records = [];

  for (let index = 0; index < 100; index++) {
    console.log(`Processing file ${index}`);

    const txtFileName = `${index}.txt`;
    const imageFileName = `${index}.jpg`;

    try {
      const response = await sendRequest(imageFileName);

      const words = response?.data?.regions[0]?.lines[0]?.words;

      const ocrPlateNumber =
        words?.reduce((pNum, val) => {
          return pNum + (val?.text || "");
        }, "") || "";

      const filePlateNumber = (await getFileData(txtFileName)).trim();

      const correct = filePlateNumber === ocrPlateNumber ? "yes" : "no";

      const correctUpper =
        filePlateNumber.toLocaleUpperCase() ===
        ocrPlateNumber.toLocaleUpperCase()
          ? "yes"
          : "no";

      records.push([
        imageFileName,
        filePlateNumber,
        ocrPlateNumber,
        correct,
        correctUpper,
      ]);

      if (index > 0 && index % 10 === 0) {
        console.log("Sleeping...");
        console.time("sleep");
        await sleep(60);
        console.timeEnd("sleep");
      }
    } catch (error) {
      console.log(error.response.data);
      await writeToCsv(records);
      process.exit(0);
    }
  }

  await writeToCsv(records);
  console.log("Csv written");
};

main();
