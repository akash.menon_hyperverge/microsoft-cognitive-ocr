# Microsoft Cognitive OCR API

## Steps to run

- Create a .env file in the project root with the following variables

```env
ENDPOINT=https://{endpoint}/vision/v3.1/ocr
OUTPUT_CSV_PATH=/path/to/output/csv
API_KEY=
IMAGE_DIRECTORY=/path/to/image/directory
FILE_DIRECTORY=/path/to/groundtruth/directory
```

- Install the dependencies
  >npm run install
- Run index.js
  >node index.js
  