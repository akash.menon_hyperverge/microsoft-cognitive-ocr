const createCsvWriter = require("csv-writer").createArrayCsvWriter;
const { OUTPUT_CSV_PATH } = process.env;

const writeToCsv = (records) => {
  const csvWriter = createCsvWriter({
    header: [
      "filename",
      "expected",
      "predicted",
      "correct",
      "uppercaseCorrect",
    ],
    path: OUTPUT_CSV_PATH,
  });

  return csvWriter.writeRecords(records);
};

module.exports = writeToCsv;
