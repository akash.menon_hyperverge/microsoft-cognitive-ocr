const fs = require("fs/promises");
const path = require("path");

const { FILE_DIRECTORY } = process.env;

const getFileData = (fileName) => {
  const filePath = path.join(FILE_DIRECTORY, fileName);
  return fs.readFile(filePath, { encoding: "utf-8" });
};

module.exports = getFileData;
